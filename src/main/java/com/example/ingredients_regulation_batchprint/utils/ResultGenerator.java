package com.example.ingredients_regulation_batchprint.utils;

/**
 * create datetime 2023/5/10
 * 响应结果生成工具
 */
public class ResultGenerator {
    private static final String DEFAULT_SUCCESS_MESSAGE = "SUCCESS";

    public static Result genSuccessResult(String message) {
        return new Result()
                .setCode(com.example.ingredients_regulation_batchprint.utils.ResultCode.SUCCESS)
                //.setMessage(DEFAULT_SUCCESS_MESSAGE);
                .setMessage(message);
    }

    public static <T> Result<T> genSuccessResult(T data) {
        return new Result<T>()
                .setCode(com.example.ingredients_regulation_batchprint.utils.ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE)
                .setData(data);
    }

    public static Result genFailResult(String message) {
        return new Result()
                .setCode(com.example.ingredients_regulation_batchprint.utils.ResultCode.FAIL)
                .setMessage(message);
    }
}
