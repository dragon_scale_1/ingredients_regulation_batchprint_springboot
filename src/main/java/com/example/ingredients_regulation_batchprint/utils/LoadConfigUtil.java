package com.example.ingredients_regulation_batchprint.utils;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * description 读取目录下面的配置文件信息
 * @author weicb
 */
public class LoadConfigUtil {

    public static Properties loadConfig(){
        Properties externalResourceFile = new Properties();
        //提前放置与程序同一目录下面的配置文件
        String jarPath = System.getProperty("user.dir") + "/basicConfiguration.properties";
        try {
            FileInputStream in = new FileInputStream(jarPath);
            externalResourceFile.load(in);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return externalResourceFile;
    }

}
