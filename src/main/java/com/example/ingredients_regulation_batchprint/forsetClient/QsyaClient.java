package com.example.ingredients_regulation_batchprint.forsetClient;

import com.alibaba.fastjson.JSONObject;
import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.Get;
import com.dtflys.forest.annotation.Query;
import com.dtflys.forest.annotation.Var;
import com.dtflys.forest.http.ForestResponse;

/**
 * Created by Enzo Cotter on 2022/3/23.
 */

@BaseRequest(

        headers = {
                "Content-Type: application/x-www-form-urlencoded"
        }
)
public interface QsyaClient {

    /**
     * 配送单批量打印
     *
     * @return
     */
    @Get(value = "http://{forestUrl}/jeecg-supplier/supplier/gysDistributionManagement/desktopExportWordToPrint")
    ForestResponse<JSONObject> queryGysDistributionManagementData(
            @Var("forestUrl") String forestUrl,
            @Query("ids") String ids,
            @Query("printType") String printType,
            @Query("title") String title,
            @Query("paperType") String paperType
    );

    /**
     * 入库单批量打印
     *
     * @return
     */
    @Get(value = "http://{forestUrl}/jeecg-school/school/xxStockManagement/desktopExportWordToPrint")
    ForestResponse<JSONObject> queryXxStockManagementData(
            @Var("forestUrl") String forestUrl,
            @Query("ids") String ids,
            @Query("printType") String printType,
            @Query("title") String title,
            @Query("paperType") String paperType
    );


}
