package com.example.ingredients_regulation_batchprint.entity;


import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

@Data
public class PrintEntity {

    /**
     * ids
     */
    @Excel(name="ids",width=15)
    private String ids;

    /**
     * printType
     */
    @Excel(name="printType",width=15)
    private String printType;

    /**
     * title
     */
    @Excel(name="title",width=15)
    private String title;

    /**
     * paperType
     */
    @Excel(name="paperType",width=15)
    private String paperType;
}
