package com.example.ingredients_regulation_batchprint;

import com.dtflys.forest.springboot.annotation.ForestScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;


/**
 * create datetime 2023/5/10
 * springboot 启动入口
 */
@SpringBootApplication
@ForestScan(basePackages = "com.example.ingredients_regulation_batchprint.forsetClient")
public class IngredientsRegulationBatchprintApplication {

    public static void main(String[] args) {
        String userHome = System.getProperties().getProperty("user.home");
        //文件保存路径
        File saveDir = new File(userHome + "\\data_qsya");
        if (!saveDir.exists()) {
            saveDir.mkdir();
        }
        SpringApplication.run(IngredientsRegulationBatchprintApplication.class, args);
    }




}
