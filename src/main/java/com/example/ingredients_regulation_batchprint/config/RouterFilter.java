package com.example.ingredients_regulation_batchprint.config;

import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by Enzo Cotter on 2023/6/1 0001.
 */
@Order(1)
@WebFilter(urlPatterns = {"/*"}, filterName = "routerFilter")
public class RouterFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        String servletPath = request.getServletPath();
        String context = request.getContextPath();
        //静态资源和接口一律放行
//路由地址一律转发到index.html页面
        if (servletPath.startsWith("/dist")) {
            req.getRequestDispatcher(context + "/index.html").forward(req, resp);
        } else {
            chain.doFilter(req, resp);
        }
    }

}
