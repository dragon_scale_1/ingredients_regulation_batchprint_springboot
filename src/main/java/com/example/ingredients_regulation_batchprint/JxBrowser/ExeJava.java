package com.example.ingredients_regulation_batchprint.JxBrowser;

import com.alibaba.fastjson.JSONObject;
import com.example.ingredients_regulation_batchprint.controller.POSTUrl;
import com.example.ingredients_regulation_batchprint.entity.PrintEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/api/batchPrint")
public class ExeJava {

    //    public final static String downLoadPath = "C:\\downLoadPath\\";
    @PostMapping(value = "/print")
    public String batchPrint(@RequestBody PrintEntity printEntity) throws Exception {
//    public static String callMe(String equipmentId, String printId, String ids, String tenantId) throws Exception {
//        //获取桌面路径
//        FileSystemView systemView = FileSystemView.getFileSystemView();
//        File homeDirectory = systemView.getHomeDirectory();
//        String downLoadPath = homeDirectory.getPath() + "\\downLoadPath\\";

//        JSONObject jsonObject = JSONObject.parseObject(o.toString());
        System.out.println(printEntity.toString());
        //获取用户路径
        String userHome = System.getProperties().getProperty("user.home");
        String downLoadPath = userHome + "\\data_qsya\\downLoad\\";

        //下载之前先去清除之前生成的文件
        File filePath = new File(downLoadPath);
        deleteFile(filePath);

        POSTUrl post = new POSTUrl();

//      JSONObject js = post.batchPrint("1706924378203721730", "distribution", "", "A4-横向");
//        JSONObject js = post.batchPrint(jsonObject.getString("ids"),
//                jsonObject.getString("printType"),
//                jsonObject.getString("title"),
//                jsonObject.getString("paperType"));
        JSONObject js = post.batchPrint(printEntity.getIds(),
                printEntity.getPrintType(),
                printEntity.getTitle(),
                printEntity.getPaperType());
        //成功则打开word
        if ("200".equals(js.get("code").toString())) {
            Desktop.getDesktop().open(new File(js.get("url").toString()));
        }
        System.out.println(js.toJSONString());
        return js.toJSONString();

    }

//    public static String getWeight() throws Exception {
//        //System.out.println(weight + "=================================");
//        return C4.weight + "";
//    }
//
//    public static void open() {
//        System.out.println("打开串口");
//        JxBrowserMain.c4.openSerialPort("打开串口");
//    }
//
//    public static void close() {
//        C4.weight = 0.00;
//        JxBrowserMain.c4.closeSerialPort("关闭串口");
//    }

    //最小化  不能用frame.setExtendedState(JFrame.ICONIFIED),不然不能恢复
//    public static void tableMin() {
//        JxBrowserMain.frame.setState(JFrame.ICONIFIED);
//    }

    //关闭程序
    public static void tableClose() {
        System.exit(0);
    }

    //关闭电脑
    public static void winShutdown() {
        try {
            // 执行关机命令：shutdown -s -t 0
            Runtime.getRuntime().exec("shutdown -s -t 0");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 打印之前删除上次的文件
     *
     * @param file
     */
    public static void deleteFile(File file) {
        //判断文件不为null或文件目录存在
        if (file == null || !file.exists()) {
            System.out.println("文件删除失败,请检查文件路径是否正确");
            return;
        }
        //取得这个目录下的所有子文件对象
        File[] files = file.listFiles();
        //遍历该目录下的文件对象
        for (File f : files) {
            //打印文件名
            String name = file.getName();
            System.out.println(name);
            //判断子目录是否存在子目录,如果是文件则删除
            if (f.isDirectory()) {
                deleteFile(f);
            } else {
                f.delete();
            }
        }
        //删除空文件夹 for循环已经把上一层节点的目录清空。
        file.delete();
    }


}
