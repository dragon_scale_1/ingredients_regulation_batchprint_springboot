//package com.example.ingredients_regulation_batchprint.JxBrowser;
//
//import java.awt.*;
//import java.awt.event.*;
//import java.awt.image.BufferedImage;
//import java.io.IOException;
//import java.lang.reflect.Field;
//import java.lang.reflect.Modifier;
//import java.math.BigInteger;
//import java.net.ServerSocket;
//import java.util.Properties;
//
//import com.example.ingredients_regulation_batchprint.config.ProgressBar;
//import com.example.ingredients_regulation_batchprint.utils.InfoUtil;
//import com.example.ingredients_regulation_batchprint.utils.LoadConfigUtil;
////import com.spire.doc.Document;
//import com.teamdev.jxbrowser.chromium.*;
//import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
//import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
//import com.teamdev.jxbrowser.chromium.swing.BrowserView;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.boot.ApplicationArguments;
//import org.springframework.boot.ApplicationRunner;
//import org.springframework.stereotype.Component;
//
//import javax.imageio.ImageIO;
//import javax.swing.*;
//
//
///**
// * Created by Enzo Cotter on 2023/5/25 0025.
// */
////@Component
//public class JxBrowserMain implements ApplicationRunner {
//
//    public static JFrame frame;
//    static boolean isOpen = false;
//    static String tenantId = "";
//    static String webUrl = "http://192.168.0.114:3100";
//    static String isDebug = "false";
//
//    private final static String TITLE = "友情提示";
//
//    //服务线程，用以控制服务器只启动一个实例
//    private static ServerSocket srvSocket = null;
//    //控制启动唯一实例的端口号，这个端口如果保存在配置文件中会更灵活
//    private static final int srvPort = 12345;
//
//
//    /**
//     * 检测系统是否只启动了一个实例
//     */
//    protected static void checkSingleInstance() {
//        try {
//            srvSocket = new ServerSocket(srvPort); //启动一个ServerSocket，用以控制只启动一个实例
//        } catch (IOException ex) {
//            if (ex.getMessage().indexOf("Address already in use: JVM_Bind") >= 0) {
//                InfoUtil test = new InfoUtil();
//                //test.show(TITLE, "这是一个弹窗测试！");
//                test.show(TITLE, "您已启动了轻食易安食材管理！");
//                System.out.println("在一台主机上同时只能启动一个进程(Only one instance allowed)。");
//            }
//            System.out.println(ex);
//            System.exit(0);
//        }
//    }
//
//    //请使用正版授权
//    static {
//        try {
//            Field e = ba.class.getDeclaredField("e");
//            e.setAccessible(true);
//            Field f = ba.class.getDeclaredField("f");
//            f.setAccessible(true);
//            Field modifersField = Field.class.getDeclaredField("modifiers");
//            modifersField.setAccessible(true);
//            modifersField.setInt(e, e.getModifiers() & ~Modifier.FINAL);
//            modifersField.setInt(f, f.getModifiers() & ~Modifier.FINAL);
//            e.set(null, new BigInteger("1"));
//            f.set(null, new BigInteger("1"));
//            modifersField.setAccessible(false);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    static int mouseAtX;
//    static int mouseAtY;
//
//    @Override
//    public void run(ApplicationArguments args) throws Exception {
//
//        //校验重复启动
//        checkSingleInstance();
//
//        //运行启动界面
////        ProgressBar progressBar = new ProgressBar();
////        progressBar.start();
//
//        Toolkit kit = Toolkit.getDefaultToolkit();
//        Dimension sc = kit.getScreenSize();
//        JWindow jf = new JWindow();
//        jf.setSize(145, 500);
////        jf.setLocation(sc.width/45,sc.height/9);
//        jf.setLocation(sc.width - jf.getWidth(), sc.height - jf.getHeight());
////        jf.setUndecorated(true);		//窗口去边框
//        jf.setAlwaysOnTop(true);        //设置窗口总在最前
//        jf.setBackground(new Color(0, 0, 0, 0));        //设置窗口背景为透明色
//        jf.addMouseListener(new MouseAdapter()        //设置窗口可拖动
//        {
//            @Override
//            public void mousePressed(MouseEvent e) {
//                mouseAtX = e.getPoint().x;
//                mouseAtY = e.getPoint().y;
//            }
//        });
//        jf.addMouseMotionListener(new MouseMotionAdapter() {
//            @Override
//            public void mouseDragged(MouseEvent e) {
//                jf.setLocation((e.getXOnScreen() - mouseAtX), (e.getYOnScreen() - mouseAtY));//设置拖拽后，窗口的位置
//            }
//        });
//
//        // 添加鼠标处理
////        jf.addMouseListener(new MouseListener() {
////            @Override
////            public void mousePressed(MouseEvent e) {
////                // do nothing
////            }
////
////            @Override
////            public void mouseEntered(MouseEvent e) {
////                // do nothing
////            }
////
////            @Override
////            public void mouseClicked(MouseEvent e) {
////                try {
////                    if (!isOpen) {
////                        if ("".equals(tenantId)) {
////                            Properties config = LoadConfigUtil.loadConfig();
////                            //机构id
////                            tenantId = config.getProperty("organization_code");
////                            main1(tenantId);
////                        }
////                        //setVisible和show功能一样
////                        frame.setVisible(true);
////                        //show方法jdk1.5之后过时
////                        //frame.show();
////                        isOpen = true;
////                    } else {
////                        isOpen = false;
////                        frame.setVisible(false);
////                        //frame.dispose();
////                        //frame.show(false);
////                        //frame.setAlwaysOnTop(true);
////                        //frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
////                        //frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
////                        //frame.setExtendedState(frame.getExtendedState());
////                    }
////
////                } catch (ClassNotFoundException ex) {
////                    ex.printStackTrace();
////                } catch (UnsupportedLookAndFeelException ex) {
////                    ex.printStackTrace();
////                } catch (InstantiationException ex) {
////                    ex.printStackTrace();
////                } catch (IllegalAccessException ex) {
////                    ex.printStackTrace();
////                }
////            }
////
////            @Override
////            public void mouseReleased(MouseEvent e) {
////                // do nothing
////            }
////
////            @Override
////            public void mouseExited(MouseEvent e) {
////                // do nothing
////            }
////        });
//
////        //按钮1
////        ImageIcon run = new ImageIcon(getClass().getResource("/static/icons/qsya.png"));            //实例化图像对象以作为按钮贴图
////        JButton PPTrun = new JButton(run);        //将上面的图像对象设置为按钮贴图
////        PPTrun.setContentAreaFilled(false);        //设置按钮背景透明
////        PPTrun.setBorderPainted(false);        //去掉按钮边框
////        PPTrun.setBounds(8, 311, 130, 58);        //设置按钮大小及位置
////
////        //按钮2
////        ImageIcon back = new ImageIcon(getClass().getResource("/static/icons/qsya.png"));            //实例化图像对象以作为按钮贴图
////        JButton PPTback = new JButton(back);        //将上面的图像对象设置为按钮贴图
////        PPTback.setContentAreaFilled(false);        //设置按钮背景透明
////        PPTback.setBorderPainted(false);        //去掉按钮边框
////        PPTback.setBounds(8, 373, 130, 58);
////        //按钮3
////        ImageIcon exit = new ImageIcon("src/xfq.png");            //实例化图像对象以作为按钮贴图
////        JButton PPTexit = new JButton(exit);        //将上面的图像对象设置为按钮贴图
////        PPTexit.setContentAreaFilled(false);        //设置按钮背景透明
////        PPTexit.setBorderPainted(false);        //去掉按钮边框
////        PPTexit.setBounds(8, 432, 130, 58);
//        //System.out.println(System.getProperty("user.dir"));
//        //设置按钮大小及位置
//        //实例化图像对象以作为窗口主题贴图
//        ImageIcon c = new ImageIcon(getClass().getResource("/static/icons/qsya.png"));
//        //ImageIcon c = new ImageIcon("/xfq.png");
//
//        JLabel l = new JLabel(c);        //把上面的主题贴图添加到标签对象里面去
//        l.setBounds(0, 136, 144, 177);        //设置标签对象大小及位置
//        JPanel p = new JPanel();
//        p.setLayout(null);
//        p.add(l);
////        p.add(PPTrun);
////        p.add(PPTback);
////        p.add(PPTexit);
//        p.setOpaque(false);
//        //System.out.println("打开悬浮球");
//        jf.getContentPane().add(p);
//        jf.show();
//
//
//    }
//
//
//    public static void main1(String tenantId) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
//
//
//        //加载配置文件
//        Properties config = LoadConfigUtil.loadConfig();
//
//        if (org.apache.commons.lang3.StringUtils.isNotBlank(config.getProperty("isDebug"))) {
//            isDebug = config.getProperty("isDebug");
//        }
//        if ("true".equals(isDebug)) {
//            BrowserPreferences.setChromiumSwitches("--remote-debugging-port=9222");
//        }
//
//        //swing下 改变窗口样式为windows风格(swing控件样式不美观解决方案)
//        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//        JPopupMenu.setDefaultLightWeightPopupEnabled(false);
//
//        //如果启动两个不同的应用的话，这里需要配置一下context路径
//        String userHome = System.getProperties().getProperty("user.home");
//        //BrowserContext context = new BrowserContext(new BrowserContextParams(userHome+"\\data_area"));
//        BrowserContext context = new BrowserContext(new BrowserContextParams(userHome + "\\data_qsya"));
//
//        //BrowserPreferences.setChromiumSwitches("--remote-debugging-port=9222");
//        Browser browser = new Browser(context);
//        BrowserView view = new BrowserView(browser);
//        frame = new JFrame("轻食易安食材管理");
//        // JFrame frame = new JFrame("乐康体检智能管理后台");
//        BufferedImage image = null;
//        try {
////                        image = ImageIO.read(frame.getClass()
////                    .getResource("/a.png"));
//            image = ImageIO.read(frame.getClass()
//                    .getResource("/static/icons/qsya.png"));
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        frame.setIconImage(image);
//
//        frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
//        //设置窗口大小
//        frame.setSize(1700, 1000);
//        //设置此 frame 的初始化最大化边界  x:左边距 y:上边距 width:宽 height:高
//        frame.setMaximizedBounds(new Rectangle(50, 50, 1700, 1000));
//        ////拉动最大化
//        frame.setMaximumSize(new Dimension(1700, 1000));
//        ////拉动最小化
//        frame.setMinimumSize(new Dimension(850, 500));
//
//        frame.setUndecorated(false);
//
//        //JFrame去掉边框
//        //frame.setTitle("检查报告检查报告");
//
//        frame.add(view, BorderLayout.CENTER);
//        frame.setAlwaysOnTop(true);
//        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
//        frame.setLocationRelativeTo(null);
//
//        frame.setState(Frame.NORMAL);
//        frame.requestFocus();
//        frame.setVisible(true);
//        browser.setContextMenuHandler(new MyContextMenuHandler(view));
//
//        String remoteDebuggingURL = browser.getRemoteDebuggingURL();
//
//        browser.addLoadListener(new LoadAdapter() {
//            @Override
//            public void onFinishLoadingFrame(FinishLoadingEvent event) {
//                if (event.isMainFrame()) {
//
//                    JSValue window = browser.executeJavaScriptAndReturnValue("window");
//                    // 给jswindows对象添加一个扩展的属性
//                    ExeJava callJava = new ExeJava();
//                    window.asObject().setProperty("callJava", callJava);
//                }
//            }
//        });
//
////        browser.addScriptContextListener(new ScriptContextAdapter() {
////            @Override
////            public void onScriptContextCreated(ScriptContextEvent event) {
////                Browser browser = event.getBrowser();
////                JSValue window = browser.executeJavaScriptAndReturnValue("window");
////                window.asObject().setProperty("callJava", new JxBrowserMain());
////            }
////        });
//
//        //防止操作失误，默认打开测试区域管理
//        if (com.dtflys.forest.utils.StringUtils.isNotBlank(config.getProperty("webUrl"))) {
//            webUrl = config.getProperty("webUrl");
//        }
//        //打开网站
//        browser.loadURL(webUrl);
//        //Specifies remote debugging port for remote Chrome Developer Tools.
//        //debug开启
//        if ("true".equals(isDebug)) {
//            Browser browser2 = new Browser();
//            BrowserView view2 = new BrowserView(browser2);
//            JFrame frame2 = new JFrame();
//            frame2.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//            frame2.add(view2, BorderLayout.CENTER);
//            frame2.setSize(700, 500);
//            frame2.setLocationRelativeTo(null);
//            frame2.setVisible(true);
//            browser2.loadURL(remoteDebuggingURL);
//        }
//
//    }
//
//
//    private static class MyContextMenuHandler implements ContextMenuHandler {
//        private final JComponent component;
//
//        private MyContextMenuHandler(JComponent parentComponent) {
//            this.component = parentComponent;
//        }
//
//        @Override
//        public void showContextMenu(final ContextMenuParams params) {
//            final JPopupMenu popupMenu = new JPopupMenu();
//            final Browser browser = params.getBrowser();
//            popupMenu.add(createMenuItem("刷新窗口", new Runnable() {
//                @Override
//                public void run() {
//                    browser.reload();
//                }
//            }));
//            final Point location = params.getLocation();
//            SwingUtilities.invokeLater(new Runnable() {
//                @Override
//                public void run() {
//                    popupMenu.show(component, location.x, location.y);
//                }
//            });
//        }
//
//        private static JMenuItem createMenuItem(String title, final Runnable action) {
//            JMenuItem reloadMenuItem = new JMenuItem(title);
//            reloadMenuItem.addActionListener(new ActionListener() {
//                @Override
//                public void actionPerformed(ActionEvent e) {
//                    action.run();
//                }
//            });
//            return reloadMenuItem;
//        }
//    }
//
//}
